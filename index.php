<?php
	header('Content-Type: text/html; charset=utf-8');
	header('Access-Control-Allow-Origin: *');
	mb_internal_encoding('UTF-8');
	setlocale(LC_ALL, 'ru_RU.UTF-8');
	ini_set('memory_limit', '-1');
	// 188.120.233.65 
	// telegram 
	// telegram12345 
	// пользователь mysql

	$connectionData['HOST'] = '188.120.233.65';
	$connectionData['USER'] = 'telegram';
	$connectionData['PASSWORD'] = 'telegram12345';
	$connectionData['BASE'] = 'telegram';

	$db = new mysqli($connectionData['HOST'], $connectionData['USER'], $connectionData['PASSWORD'], $connectionData['BASE'], "3306");
	$db->set_charset("utf8");

	if ($_GET['getTobaccos']) {
		if ($_GET['getTobaccos'] == 'GET') {
			echo json_encode(select($db, '*', 'Tobaccos', NULL));
		} else {
			$brand = ucfirst(strtolower($_GET['getTobaccos']));

			echo json_encode(select($db, 'Taste', 'Tobaccos', 'Name=\'' . $brand . '\''));
		}
		die();
	} else if ($_GET['getUser']) {
		$user = select($db, '*', 'Users', 'Name=\''.$_GET['getUser'].'\'');
		echo json_encode($user);
		die();
	} else if ($_GET['getUsers']) {
		$users = select($db, '*', 'Users', NULL);
		echo json_encode($users);
		die();
	} else if ($_GET['getSchedule']) {
		if ($_GET['getSchedule'] == 'GET') {
			$query = select($db, '*', 'UsersDays', NULL);
			$daysOfWeek = select($db, '*', 'DaysOfWeek', NULL);
			$users = select($db, '*', 'Users', NULL);
			$schedule = array();
			foreach ($query as $k => $v) {
				$user = getUserById($users, $v['UserId']);
				$day = getDayById($daysOfWeek, $v['DayId']);
				if ($schedule[$user['Name']] == NULL)
					$schedule[$user['Name']] = array();
				array_push($schedule[$user['Name']], $day['Name']);
			}
			echo json_encode($schedule);
			die();
		} else {
			$day = $_GET['getSchedule'];
			$users = select($db, 'UserId', 'UsersDays', ' DayId=\'' . $day . '\'');
			echo json_encode($users);
			die();
		}
	} else if ($_POST['setSchedule']) {
		$user = $_POST['user'];
		$userId = select($db, 'Id', 'Users', 'Name=\'' . $user . '\'')[0]['Id'];
		// echo $userId; die();
		delete($db, 'UsersDays', 'UserId=\'' . $userId . '\'');
		foreach ($_POST['setSchedule'] as $k => $v) {
			insert($db, 'UsersDays', 'UserId, DayId', $userId . ', ' . $v);
		}
		die('Edited');
	} else if ($_POST['addTobacco']) {
		$name = $_POST['Name'];
		$taste = $_POST['Taste'];
		insert($db, 'Tobaccos', 'Name, Taste, Available', '\'' . $name . '\'' . ', ' . '\'' . $taste . '\', \'1\'');
		die('Success');
	} else if ($_POST['availableTobacco']) {
		$id = $_POST['availableTobacco'];
		$available = $_POST['available'];
		update($db, 'Tobaccos', 'Available=\''. $available . '\'', 'Id=\'' . $id . '\'');
		// delete($db, 'Tobaccos', 'Id=\'' . $id . '\'');
		die('Edited');
	} else if ($_GET['getWorkHours']) {
		$day = $_GET['getWorkHours'];
		$workHours = select($db, '*', 'WorkHours', NULL);
		echo json_encode($workHours); 
		die();
	} else if ($_POST['updateWorkHours']) {
		$day = $_POST['day'];
		$timeOpen = $_POST['timeOpen'];
		$timeClose = $_POST['timeClose'];
		update($db, 'WorkHours', 'TimeOpen=\'' . $timeOpen . '\', TimeClose=\'' . $timeClose . '\'', 'Day=\'' . $day . '\'');
		die('Success');
	}

	function getDayById($days, $id) {
		foreach ($days as $k => $v) {
			if ($v['Id'] == $id)
				return $v;
		}
	}

	function getUserById($users, $id) {
		foreach ($users as $k => $v) {
			if ($v['Id'] == $id)
				return $v;
		}
	}

	function insert($db, $table, $fields, $values) {
		$query = 'INSERT INTO ' . $table . ' (' . $fields . ') VALUES (' . $values . ')';
		// echo $query; die();
		$queryResult = $db->query($query);
		return $queryResult;
	}

	function update($db, $table, $set, $where) {
		$query = 'UPDATE ' . $table . ' SET ' . $set . ' WHERE ' . $where;
		// echo $query; die();
		$queryResult = $db->query($query);
		return $queryResult;
	}

	function select($db, $what, $where, $condition) {
		if (isset($condition)) 
			$query = 'SELECT ' . $what . ' FROM ' . $where . ' WHERE ' . $condition;
		else
			$query = 'SELECT ' . $what . ' FROM ' . $where;
		// echo $query; die();
		$queryResult = $db->query($query);
		$result = array();
		while ($row = $queryResult->fetch_assoc()) {
			array_push($result, $row);
		}
		return $result;
	}

	function delete($db, $from, $where) {
		$query = 'DELETE FROM ' . $from . ' WHERE ' . $where;
		$queryResult = $db->query($query);
		return $queryResult;
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Gustoy</title>
	<script src="/template/js/jquery.min.js" type="text/javascript"></script>
	<script src="/template/js/bootstrap.min.js" type="text/javascript"></script>
	<link href="template/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript">
		$(document).ready(function () {
			var tobaccoQuery = $.getJSON("index.php/?getTobaccos=GET")
				.done(function(data) {
					var tobaccoBrands = new Array();
					$('#tobaccoBrands').append('<option></option>');
					$.each(data, function(key, value) {
						if (value['Available'] == '1') {
							var checkbox = '<input type="checkbox" name="availableTobacco" id="' + value['Id'] + '" checked>';
						} else {
							var checkbox = '<input type="checkbox" name="availableTobacco" id="' + value['Id'] + '">';
						}
						if ($.inArray(value['Name'], tobaccoBrands) == -1) {
							$('#tobaccoBrands').append('<option>' + value['Name'] + '</option>');
							tobaccoBrands.push(value['Name']);	
						}
						$('#tobacco-table').append('<tr class="' + value['Name'] + '" name="tobacco-table-tr"><td>'+ value['Id'] +'</td><td>'+ value['Name'] +'</td><td>'+ value['Taste'] +'</td><td>' + checkbox + '</td></tr>');
					});
				})
				.fail(function() {
					alert("error");
				});
		
			var scheduleQuery = $.getJSON("index.php/?getSchedule=GET")
				.done(function(data) {
					$.each(data, function(key, value) {
						var days = '';
						$.each(value, function(k, v) {
							days += v + ' ';
						});
						var button = '<button type="button" class="btn btn-default" name="editSchedule" user="' + key + '"><span class="glyphicon glyphicon-pencil"></span></button>';
						$('#schedule-table').append('<tr><td>'+ key +'</td><td>'+ days +'</td><td>' + button + '</td></tr>');
					});
				})
				.fail(function() {
					alert("error");
				});

			var workHoursQuery = $.getJSON("index.php/?getWorkHours=GET")
				.done(function(data) {
					var days = {
						"1" : "Понедельник",
						"2" : "Вторник",
						"3" : "Среда",
						"4" : "Четверг",
						"5" : "Пятница",
						"6" : "Суббота",
						"7" : "Воскресенье"
					};
					$.each(data, function(key, value) {
						var day = '<span>' + days[value['Day']] + '</span>';
						var openTime = '<input type="text" class="form-control" value="' + value['TimeOpen'] + '" id="TimeOpen' + value['Day'] + '">';
						var closeTime = '<input type="text" class="form-control" value="' + value['TimeClose'] + '" id="TimeClose' + value['Day'] + '">';
						var acceptButton = '<button class="btn btn-default" name="updateWorkHours" id="' + value['Day'] + '"><span class="glyphicon glyphicon-ok"></span></button>';
						$('#workhours-table').append('<tr><td>'+ day +'</td><td>'+ openTime +'</td><td>' + closeTime + '</td><td>' + acceptButton + '</td></tr>');
					});
				})
				.fail(function() {
					alert("error");
				}); 

			$('#addTobaccoButton').click(function(){
				var Name = $("input[tobaccoInfo='Name']").val();
				$.each($("input[tobaccoInfo='Taste'"), function(){
					var Taste = $(this).val();
					$.post("index.php", { 
						addTobacco: "Add", 
						Name: Name,
						Taste: Taste 
					}).done(function(data){
						alert(data);
						$('#addTobacco').modal('hide');
						location.reload();
					});
				});
			});

			$('#tobaccoBrands').change(function(){
				var brand = $(this).val();
				if (brand != '') {
					$('tr[name=tobacco-table-tr]').not('.' + brand).hide();
					$('.' + brand).show();
				} else {
					$('tr').show();
				}

			});

			$('body').on('click', 'button[name="updateWorkHours"]', function(){
				var day = $(this).attr("id");
				var timeOpen = $('#TimeOpen' + day).val();
				var timeClose = $('#TimeClose' + day).val();
				$.post("index.php", { 
					updateWorkHours: "Update",
					day: day,
					timeOpen : timeOpen,
					timeClose : timeClose
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});

			$('body').on('click', 'input[name="availableTobacco"]', function(){
				var id = $(this).attr('id');
				var available = 0;
				if ($(this).is(':checked')) {
					available = 1;
				}
				$.post("index.php", { 
					availableTobacco: id,
					available: available,
				}).done(function(data){
					// alert(data);
					location.reload();
				});			
			});

			$('body').on('click', 'button[name="editSchedule"]', function(){
				$('.userScheduleName').text('Изменить расписание (' + $(this).attr('user') + ')');
				$('#acceptScheduleButton').attr('name', $(this).attr('user'));
				$('#editSchedule').modal('show');
			});

			$('#acceptScheduleButton').click(function () {
				var days = [];
				$("input[type=checkbox]").each(function(index) {
					if ($(this).is(':checked'))
						days.push($(this).val());
				});
				$.post("index.php", {
					setSchedule: days,
					user: $('#acceptScheduleButton').attr('name'),
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});

			$('input[name=showAddTobaccoModal]').click(function(){
				$('input[tobaccoInfo=Name]').val($('#tobaccoBrands').val());
				$('#addTobacco').modal();
			});

			$('input[name=addTobaccoTaste]').click(function(){
				$('div[name=addTobaccoModal]').append('<input type="text" class="form-control" placeholder="Вкус (название)" tobaccoInfo="Taste">');
			});

		});
	</script>
</head>
<body>
	<div class="content">
		<div class="row">
			<div class="col-md-3">
				<h3><span class="label label-default">Табаки</span></h3>
				<div class="form-group">
					<label for="sel1">Марка табака:</label>
					<select class="form-control" id="tobaccoBrands">
					</select>
				</div>
				<table class="table" id="tobacco-table">
					<tr>
						<th>ID</th>
						<th>Название</th>
						<th>Вкус</th>
						<th>Наличие</th>
					</tr>
				</table>
				<input type="button" class="btn btn-primary col-md-2" value="+" name="showAddTobaccoModal">
			</div>
			<div class="col-md-3">
				<h3><span class="label label-default">Смены</span></h3>
				<table class="table" id="schedule-table">
					<tr>
						<th>Кальянщик</th>
						<th>Дни недели</th>
						<th>Редактировать</th>
					</tr>
				</table>
			</div>
			<div class="col-md-3">
				<h3><span class="label label-default">Расписание работы</span></h3>
				<table class="table" id="workhours-table">
					<tr>
						<th>День недели</th>
						<th>Время открытия</th>
						<th>Время закрытия</th>
						<th>Применить</th>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<!-- Tobacco Modal -->
	<div id="addTobacco" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Добавить табак</h4>
				</div>
				<div class="modal-body" name="addTobaccoModal">
					<input type="text" class="form-control" placeholder="Марка" tobaccoInfo="Name">
					<input type="text" class="form-control" placeholder="Вкус (название)" tobaccoInfo="Taste">
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" value="Добавить вкус" name="addTobaccoTaste">
					<button type="button" class="btn btn-primary" id="addTobaccoButton">Добавить</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Schedule Modal -->
	<div id="editSchedule" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title userScheduleName">Изменить смены</h4>
				</div>
				<div class="modal-body">
					<div class="checkbox">
						<label><input type="checkbox" value="1">Понедельник</label>
					</div>
					<div class="checkbox">
						<label><input type="checkbox" value="2">Вторник</label>
					</div>
					<div class="checkbox">
						<label><input type="checkbox" value="3">Среда</label>
					</div>
					<div class="checkbox">
						<label><input type="checkbox" value="4">Четверг</label>
					</div>
					<div class="checkbox">
						<label><input type="checkbox" value="5">Пятница</label>
					</div>
					<div class="checkbox">
						<label><input type="checkbox" value="6">Суббота</label>
					</div>
					<div class="checkbox">
						<label><input type="checkbox" value="7">Воскресенье</label>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="acceptScheduleButton">Сохранить</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>